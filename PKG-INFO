Metadata-Version: 1.1
Name: graphite-web
Version: 1.1.8
Summary: Enterprise scalable realtime graphing
Home-page: http://graphiteapp.org/
Author: Chris Davis
Author-email: chrismd@gmail.com
License: Apache Software License 2.0
Description: # Graphite-Web
        
        [![Codacy Badge](https://api.codacy.com/project/badge/Grade/5e94ef79c2ea441aaf209cfb2851849e)](https://www.codacy.com/app/graphite-project/graphite-web?utm_source=github.com&utm_medium=referral&utm_content=graphite-project/graphite-web&utm_campaign=badger)
        [![Build Status](https://travis-ci.org/graphite-project/graphite-web.png?branch=master)](https://travis-ci.org/graphite-project/graphite-web)
        [![FOSSA Status](https://app.fossa.io/api/projects/git%2Bhttps%3A%2F%2Fgithub.com%2Fgraphite-project%2Fgraphite-web.svg?type=shield)](https://app.fossa.io/projects/git%2Bhttps%3A%2F%2Fgithub.com%2Fgraphite-project%2Fgraphite-web?ref=badge_shield)
        [![codecov](https://codecov.io/gh/graphite-project/graphite-web/branch/master/graph/badge.svg)](https://codecov.io/gh/graphite-project/graphite-web)
        
        ## Overview
        
        Graphite consists of three major components:
        
        1. Graphite-Web, a Django-based web application that renders graphs and dashboards
        2. The [Carbon](https://github.com/graphite-project/carbon) metric processing daemons
        3. The [Whisper](https://github.com/graphite-project/whisper) time-series database library
        
        ![Graphite Components](https://github.com/graphite-project/graphite-web/raw/master/webapp/content/img/overview.png "Graphite Components")
        
        ## Installation, Configuration and Usage
        
        Please refer to the instructions at [readthedocs](http://graphite.readthedocs.io/).
        
        ## License
        
        Graphite-Web is licensed under version 2.0 of the Apache License. See the [LICENSE](https://github.com/graphite-project/graphite-web/blob/master/LICENSE) file for details.
        
Platform: UNKNOWN
Classifier: Intended Audience :: Developers
Classifier: Natural Language :: English
Classifier: License :: OSI Approved :: Apache Software License
Classifier: Programming Language :: Python
Classifier: Programming Language :: Python :: 2
Classifier: Programming Language :: Python :: 2.7
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.4
Classifier: Programming Language :: Python :: 3.5
Classifier: Programming Language :: Python :: 3.6
Classifier: Programming Language :: Python :: 3.7
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: Implementation :: CPython
Classifier: Programming Language :: Python :: Implementation :: PyPy
